from django.contrib import admin
from django.urls import path, include, re_path
from django.conf import settings
from django.conf.urls.static import static
from Projects import views as project_views
from django_filters.views import FilterView
from . import models
from django.conf.urls import handler404, handler500, handler403
app_name = "Projects"

handler404 = 'Projects.views.handler404'
handler500 = 'Projects.views.handler500'

urlpatterns = [
    # path('details/<int:project_id>', project_views.ProjectDetailView,  name='project_details'),
    path('add_project/', project_views.AddProjectView.as_view(), name='add_project'),
    path('add_project_form/', project_views.add_project_form, name='add_project_form'),
    path('recommendation_form/<str:slug>', project_views.recommendation_form, name='recommendation_form'),
    path('recommendation_view/<str:slug>', project_views.view_recommendations, name='recommendation_view'),
    path('acceptrecom/<int:recom_id>', project_views.acceptrecom, name="acceptrecom"),
    path('rejectrecom/<int:recom_id>', project_views.rejectrecom, name="rejectrecom"),
    path('challenge_grants/', project_views.ChallengeGrants.as_view(), name='challenge_grants'),
    # path('delete_view/', project_views.DeleteProjectView.as_view(), name='delete_view'),
    path('delete_project/<str:slug>/', project_views.delete_project, name='delete_project'),
    path('edit_project/<str:slug>', project_views.EditView.as_view(), name='edit_project'),
    path('edit_project_submit/<str:slug>', project_views.edit_project_form, name='edit_project_form'),
    path('deadline_extension/<str:slug>', project_views.deadline_extension, name='deadline_extension'),
    path('dead_req_det/<str:slug>', project_views.dead_request_details, name='dead_req_det'),
    path('accept_dead/<str:slug>', project_views.accept_dead, name='accept_dead'),
    path('reject_dead/<str:slug>', project_views.reject_dead, name='reject_dead'),
    #path('project_list/', project_views.ProjectListView, name='projectlistview'),
    path('add_book/<str:slug>', project_views.add_book, name='add_book'),
    path('rem_book.<str:slug>', project_views.rem_book, name='rem_book'),

    path('mentorship_form/<str:slug>', project_views.mentor_form, name='mentorship_form'),
    path('mentorship_view/<str:slug>', project_views.view_mentorships, name='mentorship_view'),
    path('acceptmentor/<int:mentor_id>', project_views.acceptmentor, name="acceptmentor"),
    path('rejectmentor/<int:mentor_id>', project_views.rejectmentor, name="rejectmentor"),

    path('internship_form/<str:slug>', project_views.intern_form, name='internship_form'),
    path('internship_view/<str:slug>', project_views.view_internships, name='internship_view'),
    path('acceptintern/<int:intern_id>', project_views.acceptintern, name="acceptintern"),
    path('rejectintern/<int:intern_id>', project_views.rejectintern, name="rejectintern"),

    path('collaborator_form/<str:slug>', project_views.colab_form, name='collaborator_form'),
    path('internship_view/<str:slug>', project_views.view_collaborations, name='collaborator_view'),
    path('acceptintern/<int:colab_id>', project_views.acceptcolab, name="acceptcolab"),
    path('rejectintern/<int:colab_id>', project_views.rejectcolab, name="rejectcolab"),

]

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
