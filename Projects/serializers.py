from rest_framework import serializers
from . import models


class ProjectSerializer(serializers.ModelSerializer):
    class Meta():
        model  = models.ProjectDetails
        fields = ['project_id','title', 'description', 'abstract']

class SearchSerializer(serializers.ModelSerializer):
    details = ProjectSerializer()
    
    class Meta():
        model = models.ProjectTags
        fields = ['project_id', 'tag', 'details']


