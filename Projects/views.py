import datetime
from re import split

from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import HttpResponse
from django.shortcuts import render, get_object_or_404, redirect
from django.template.loader import render_to_string
from django.urls import reverse
from django.utils import timezone
from django.http import HttpResponseRedirect
from Accounts import forms
from Projects import forms as project_forms
from Accounts.models import CustomUser, History
from Projects.forms import RecommendationForm, EditProjectForm
from Projects.models import ProjectDetails, References, BankInformation, ProjectTags, Collaborators, Contributors, \
    Recommendations, Bookmarks, Mentors, Interns
from django.contrib.auth import get_user_model
from Projects.models import References
import os
from sendgrid import SendGridAPIClient
from sendgrid.helpers.mail import Mail
from pyshorteners import Shortener
from django.views.generic import ListView
from rest_framework.viewsets import generics
from . import serializers
from . import filters
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework.filters import OrderingFilter, SearchFilter


API_Key = 'AIzaSyDzcS4JR1ZfMxzP43o-syrR9-JHlxpl3As'


# from Projects.forms import ProjectForm

# Create your views here.
from django.views.generic import TemplateView


User = get_user_model()


# class ProjectListView(generics.ListAPIView):
#     serializer_class = serializers.ProjectSerializer
#     queryset = ProjectDetails.objects.all()
#     filter_backends = (DjangoFilterBackend, OrderingFilter, SearchFilter)
#     filter_fields = ('title','description',)
#     # ordering_fields = ('',)
#     search_fields = ('title', 'description',)


def handler404(request, exception):
    return HttpResponseRedirect('/user_accounts/login/')

def handler500(request):
    return HttpResponseRedirect('user_accounts/login/')


def ProjectDetailView(request, username, slug):

    url =  "oversight.in{}".format(request.get_full_path())

    try:
        current_project = ProjectDetails.objects.get(project_slug=slug)
    except Exception as e:
        return HttpResponse("Such a Project Does Not Exist")
    user = CustomUser.objects.get(username__exact=current_project.user.username)
    try:
        Cont = Contributors.objects.filter(project_id=current_project.project_id)[:10]
    except Exception as e:
        pass

    try:
        Collabs = Collaborators.objects.filter(project_id=current_project.project_id).filter(approved=True)
    except Exception as e:
        pass

    try:
        Recoms = Recommendations.objects.filter(approved=True).filter(project_id=current_project.project_id).order_by('date_written')
    except Exception as e:
        pass
    try:
        viewing_user = CustomUser.objects.get(username=request.user.username)
        viewing_user_bookmarks = Bookmarks.objects.filter(user=viewing_user)

    except Exception as e:
        viewing_user = None
        viewing_user_bookmarks = None

    if viewing_user_bookmarks is not None:
        if current_project.project_id in viewing_user_bookmarks.values_list('project_id__project_id', flat=True):
            booked = True
        else:
            booked = False
    else:
        booked = None
    project_bookmarks = Bookmarks.objects.filter(project_id__project_slug=current_project.project_slug)
    Ints = Interns.objects.filter(project_id=current_project.project_id).filter(approved=True)

    if user.username == username:
        return render(request, 'Projects/project_details.html', {'short_url': url, 'user':user, 'Project': current_project, 'Recommendations':Recoms, 'Collaborators':Collabs, 'viewing_user':viewing_user, 'booked':booked, 'project_bookmarks': project_bookmarks, 'Interns':Ints})
    else:
        return HttpResponse("Unauthorized")


class AddProjectView(LoginRequiredMixin, TemplateView):

    login_url = '/login/'
    redirect_field_name = None

    template_name = 'Projects/addproject.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['colablist'] = CustomUser.objects.exclude(username=self.request.user.username)
        context['user'] = CustomUser.objects.get(username=self.request.user.username)
        return context


@login_required(login_url = '/login/', redirect_field_name=None)
def add_project_form(request):
    if request.method == 'POST':
        form = project_forms.AddProjectForm(request.POST)
        print("form received")
        print(form.errors)
        if form.is_valid():
            sluglist = ProjectDetails.objects.filter(user=request.user)
            sluglist = sluglist.values_list('project_slug', flat=True)
            if form.cleaned_data['project_slug'] not in sluglist:
                print("Form Cleaned")
                project = ProjectDetails()
                project.user = CustomUser.objects.get(username=request.user.username)
                project.title = form.cleaned_data['title']
                project.project_slug = form.cleaned_data['project_slug'].lower()

                project.abstract = form.cleaned_data['abstract']
                project.description = form.cleaned_data['description']
                project.significance = form.cleaned_data['significance']
                project.additional_details = form.cleaned_data['additional_details']
                project.target_amount = form.cleaned_data['target_amount']
                # s = "{}-{}-{}".format(form.cleaned_data['deadline_year'], form.cleaned_data['deadline_month'], form.cleaned_data['deadline_day'])
                # deadline = datetime.datetime.strptime(s,"%Y-%m-%d").date()
                # project.deadline = deadline
                project.codebase = form.cleaned_data['codebase']
                project.dataset = form.cleaned_data['dataset']
                project.intellectual_property = form.cleaned_data['intellectual_property']
                project.reference1 = form.cleaned_data['reference1']
                project.reference2 = form.cleaned_data['reference2']
                project.reference3 = form.cleaned_data['reference3']
                project.affiliation = form.cleaned_data['affiliation']
                if form.cleaned_data['accept_collaborators']:
                    project.accept_collaborators = form.cleaned_data['accept_collaborators']
                else:
                    project.accept_collaborators = 'off'

                if form.cleaned_data['accept_interns']:
                    project.accept_interns = form.cleaned_data['accept_interns']
                else:
                    project.accept_interns = 'off'

                if form.cleaned_data['accept_mentors']:
                    project.accept_mentors = form.cleaned_data['accept_mentors']
                else:
                    project.accept_mentors = 'off'
                project.save()
                tag_list = form.cleaned_data['tags'].split(',')
                for tag in tag_list:
                    proj_tag = ProjectTags()
                    proj_tag.project_id = ProjectDetails.objects.get(project_id=project.project_id)
                    proj_tag.tag = tag
                    proj_tag.save()

                colab_list = form.cleaned_data['collaborators'].split(',')

                for col in colab_list:
                    colab = Collaborators()
                    print(col)
                    colab.project_id = ProjectDetails.objects.get(project_id=project.project_id)
                    try:
                        colab.requesting_user = CustomUser.objects.get(username=col)
                    except Exception as e:
                        continue
                    colab.approved = True
                    colab.save()
                hist = History()
                hist.project_id = project
                hist.user = request.user
                hist.type_of_action = "Creation"
                hist.save()
                return redirect('project_details', username=request.user.username, slug=project.project_slug )
            else:
                messages.error(request, "This slug is already in use for one of your projects")
                return render(request, 'Projects/addproject.html', {'errors': form.errors,
                                                                    'colablist': CustomUser.objects.all().exclude(
                                                                        username=request.user.username),
                                                                    'user': CustomUser.objects.get(
                                                                        username=request.user.username)})

        else:
            return render(request, 'Projects/addproject.html', {'errors':form.errors, 'colablist': CustomUser.objects.all().exclude(username=request.user.username), 'user': CustomUser.objects.get(username=request.user.username)})

@login_required(login_url='/login/', redirect_field_name=None)
def recommendation_form(request, slug):
    if request.method == 'POST' and request.user != ProjectDetails.objects.get(project_slug=slug).user:
        form = RecommendationForm(request.POST)
        proj = ProjectDetails.objects.get(project_slug=slug)
        if form.is_valid():
            reco = Recommendations()
            reco.project_id = ProjectDetails.objects.get(project_slug=slug)
            reco.writing_user = CustomUser.objects.get(username=request.user.username)
            reco.Text = form.cleaned_data['Text']
            reco.save()

            mail_subject = 'A New Recommendation for {}'.format(reco.project_id.title)
            body = render_to_string('Projects/emails/recom_mail.html', {
                'user': request.user.username,
                'Project': reco.project_id,
                'recom': reco,
            })
            to_email = proj.user.email
            message = Mail(
                from_email='sameeranbandishti93@gmail.com',
                to_emails=to_email,
                subject=mail_subject,
                html_content=body
            )
            try:
                sg = SendGridAPIClient(os.environ.get('SENDGRID_API_KEY'))
                response = sg.send(message)
                print(response.status_code)
                print(response.body)
                print(response.headers)
            except Exception as e:
                print(e.message)



            return redirect('project_details',username=proj.user.username ,slug=slug)
        else:
            return HttpResponse("Invalid Operation")

    else:
        return HttpResponse("You Can't Recommend Yourself! -_-")

@login_required(login_url='/login/', redirect_field_name=None)
def view_recommendations(request, slug):
        project = ProjectDetails.objects.get(project_slug=slug)
        recommendations = Recommendations.objects.filter(project_id__project_slug=slug).filter(approved=False)

        if request.user == project.user:
            return render(request,'Projects/recom_view.html', {'project':project, 'recommendations':recommendations})
        else:
            return redirect('login')

@login_required(login_url='/login/', redirect_field_name=None)
def acceptrecom(request, recom_id):
    recom = Recommendations.objects.get(recom_id=recom_id)
    recom.approved = True
    recom.save()
    hist = History()
    hist.user = request.user
    hist.project_id = recom.project_id
    hist.type_of_action = "Recom"
    hist.save()
    return redirect('Projects:recommendation_view', slug=recom.project_id.project_slug)

@login_required(login_url='/login/', redirect_field_name=None)
def rejectrecom(request, recom_id):
    recom = Recommendations.objects.get(recom_id=recom_id)
    slug = recom.project_id.project_slug
    recom.delete()
    return redirect('Projects:recommendation_view', slug=slug)

class BrowseProjects(TemplateView):


    template_name = 'Projects/browse_projects.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['current_user'] = CustomUser.objects.get(username=self.request.user.username)
        return context

class ChallengeGrants(LoginRequiredMixin, TemplateView):

    login_url = '/login/'
    redirect_field_name = None

    template_name = 'Projects/challenge_grants.html'

class EditView(LoginRequiredMixin, TemplateView):

    login_url = '/login/'
    redirect_field_name = None

    template_name = 'Projects/edit_project.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        proj = ProjectDetails.objects.get(project_slug=self.kwargs['slug'])
        user = CustomUser.objects.get(username=proj.user.username)

        context['user'] = user
        context['colablist'] = Collaborators.objects.filter(project_id__project_slug=proj.project_slug)
        context['taglist'] = ProjectTags.objects.filter(project_id__project_slug=proj.project_slug)
        context['Project'] = proj
        return context


@login_required(login_url='/login/', redirect_field_name=None)
def edit_project_form(request, slug):
    if request.method == 'POST':
        proj = ProjectDetails.objects.get(project_slug=slug)
        if proj.user == request.user:
            form = EditProjectForm(request.POST)
            if form.is_valid():
                if form.cleaned_data['title']:
                    proj.title = form.cleaned_data['title']
                if form.cleaned_data['abstract']:
                    proj.abstract = form.cleaned_data['abstract']
                if form.cleaned_data['description']:
                    proj.description = form.cleaned_data['description']
                if form.cleaned_data['significance']:
                    proj.significance = form.cleaned_data['significance']
                if form.cleaned_data['additional_details']:
                    proj.additional_details = form.cleaned_data['additional_details']
                if form.cleaned_data['reference1']:
                    proj.reference1 = form.cleaned_data['reference1']
                if form.cleaned_data['reference2']:
                    proj.reference2 = form.cleaned_data['reference2']
                if form.cleaned_data['reference3']:
                    proj.reference3 = form.cleaned_data['reference3']
                if form.cleaned_data['codebase']:
                    proj.codebase = form.cleaned_data['codebase']
                if form.cleaned_data['dataset']:
                    proj.dataset = form.cleaned_data['dataset']
                if form.cleaned_data['intellectual_property']:
                    proj.intellectual_property = form.cleaned_data['intellectual_property']
                proj.accept_collaborators = form.cleaned_data['accept_collaborators']
                proj.accept_interns = form.cleaned_data['accept_interns']
                proj.accept_mentors = form.cleaned_data['accept_mentors']
                if form.cleaned_data['affiliation']:
                    proj.affiliation = form.cleaned_data['affiliation']

                proj.save()
                return redirect('project_details', username=request.user, slug=proj.project_slug)
            else:
                colablist = Collaborators.objects.filter(project_id__project_slug=proj.project_slug)
                taglist = ProjectTags.objects.filter(project_id__project_slug=proj.project_slug)
                return render(request, 'Projects/edit_project.html', {'errors':form.errors, 'user': request.user, 'colablist': colablist, 'taglist':taglist, 'Project':proj })
        else:
            return HttpResponse("You Are Not Authorized To Perform That Action")


@login_required(login_url='/login/', redirect_field_name=None)
def delete_project(request, slug):
    if request.method =='POST':
        proj = ProjectDetails.objects.get(project_slug=slug)
        if request.POST['userslug'] == slug and proj.user == request.user:
            proj.delete()
            return redirect('dashboard')
        else:
            return HttpResponse("You Are Not Authorized To Perform This Action")


@login_required(login_url='/login/', redirect_field_name=None)
def deadline_extension(request, slug):
    if request.method == 'POST':
        proj = ProjectDetails.objects.get(project_slug=slug)
        if request.user == proj.user:
            s = "{}-{}-{}".format(request.POST['new_dead_year'], request.POST['new_dead_month'],
                                  request.POST['new_dead_day'])
            proj.new_dead = datetime.datetime.strptime(s, "%Y-%m-%d").date()


            proj.save()
            hist = History()
            hist.user = request.user
            hist.project_id = proj
            hist.time_of_action = timezone.now()
            hist.type_of_action = "Requested Deadline Extension"
            hist.save()
            mail_subject = 'Deadline Extension Request'
            body = render_to_string('Projects/emails/dead_mail.html', {
                'user': request.user,
                'Project': proj,
                'new_dead': proj.new_dead,
            })
            to_email = 'sameeranbandishti93@gmail.com'
            message = Mail(
                from_email='sameeranbandishti93@gmail.com',
                to_emails=to_email,
                subject=mail_subject,
                html_content=body
            )
            try:
                sg = SendGridAPIClient(os.environ.get('SENDGRID_API_KEY'))
                response = sg.send(message)
                print(response.status_code)
                print(response.body)
                print(response.headers)
            except Exception as e:
                print(e)

            return redirect('project_details', slug=proj.project_slug, username=proj.user.username)


@login_required(login_url='/login/', redirect_field_name=None)
def dead_request_details(request, slug):
    if request.user.is_superuser:
        proj = ProjectDetails.objects.get(project_slug=slug)
        if proj.new_dead is not None:
            new_dead = proj.new_dead
        else:
            return HttpResponse("No Such Request Was Made")
        proj_hist = History.objects.filter(project_id__project_slug=slug)
        user_hist = History.objects.filter(user__username=proj.user.username)
        return render(request, 'Projects/dead_request_details.html', {'Project':proj, 'new_dead':new_dead, 'proj_hist':proj_hist, 'user_hist':user_hist})
    else:
        return HttpResponse("You Are Not Authorized To Perform This Action")


@login_required(login_url='/login/', redirect_field_name=None)
def accept_dead(request, slug):
    pro = ProjectDetails.objects.get(project_slug=slug)
    if request.user.is_superuser:
        pro.deadline = pro.new_dead
        pro.new_dead = None
        pro.save()
        hist = History()
        hist.user = pro.user
        hist.project_id = pro
        hist.type_of_action = "Deadline Extension Accepted"
        hist.time_of_action = timezone.now()
        hist.save()
        return redirect('project_details', username=pro.user.username, slug=pro.project_slug)
    else:
        return HttpResponse("You Are Not Authorized To Perform This Action")

@login_required(login_url='/login/', redirect_field_name=None)
def reject_dead(request, slug):
    pro = ProjectDetails.objects.get(project_slug=slug)
    if request.user.is_superuser:
        pro.new_dead = None
        pro.save()
        hist = History()
        hist.user = pro.user
        hist.project_id = pro
        hist.type_of_action = "Deadline Extension Rejected"
        hist.time_of_action = timezone.now()
        hist.save()
    else:
        return HttpResponse("You Are Not Authorized To Perform This Action")
#
# class DeleteProjectView(TemplateView):
#     template_name = 'Projects/project_delete.html'
#
#
# class EditProjectView(TemplateView):
#     template_name = 'Projects/edit_project.html'
#
#     def get_context_data(self, **kwargs):
#         context = super().get_context_data()
#         project = ProjectDetails.objects.get(project_slug=)
#         current_user =
#         context['user'] =






# def browse_projects(request):
#     if request.method == 'POST':
#         project_list = ProjectDetails.objects.all()
#         project_filter = filters.ProjectFilter(request.POST, queryset=project_list)
#         return render(request, 'Projects/browse_projects.html', {'filter': project_filter})

def browse_projects(request):
    if request.method == 'POST':
        project_list = ProjectDetails.objects.all()
        if request.POST['title_search'] != "":
            project_list = project_list.filter(title__contains=request.POST['title_search'])
        if request.POST['user_search'] != "":
            user = CustomUser.objects.get(username__exact=request.POST['user_search'])
            project_list = project_list.filter(user=user)
        # if request.POST['tag_search'] != "":
        #     project_list = project_list.filter(tags__tag=request.POST['tag_search'])
        if request.POST['oppo_search'] != "":
            if request.POST['oppo_search'] == "collaborator":
                project_list = project_list.filter(accept_collaborators='on')
            if request.POST['oppo_search'] == "mentor":
                project_list = project_list.filter(accept_mentors='on')
            if request.POST['oppo_search'] == "intern":
                project_list = project_list.filter(accept_interns='on')
        users = CustomUser.objects.all()
        all_projects = ProjectDetails.objects.all()
        try:
            current_user = CustomUser.objects.get(username=request.user.username)
        except Exception as e:
            current_user = None
            print(e)
        return render(request, 'Projects/browse_projects.html', {'project_list':project_list,'users': users, 'all_projects':all_projects, 'current_user':current_user})


    else:
        users = CustomUser.objects.all()
        all_projects = ProjectDetails.objects.all()
        try:
            current_user = CustomUser.objects.get(username=request.user.username)
        except Exception as e:
            current_user = None
            print(e)
        # all_tags = ProjectTags.objects.
        return render(request, 'Projects/browse_projects.html', {'users': users, 'all_projects':all_projects, 'current_user':current_user})

@login_required(login_url='/login/', redirect_field_name=None)
def add_book(request, slug):
    book = Bookmarks()
    book.user = request.user
    proj = ProjectDetails.objects.get(project_slug=slug)
    book.project_id = proj
    book.save()
    message =  {'header':book.project_id.title, 'body':'bookmarked'}# "`{}` bookmarked.".format(book.project_id.title)
    messages.info(request, message)
    return redirect('project_details', username=proj.user.username, slug=slug )


@login_required(login_url='/login/', redirect_field_name=None)
def rem_book(request, slug):
    book = Bookmarks.objects.filter(user=request.user).get(project_id__project_slug=slug)
    book.user = request.user
    proj = ProjectDetails.objects.get(project_slug=slug)
    message =  {'header':book.project_id.title, 'body':'un-bookmarked'} #"`{}` removed from your bookmarks.".format(book.project_id.title)
    book.delete()
    messages.info(request, message)
    return redirect('project_details', username=proj.user.username, slug=slug )

@login_required(login_url='/login/', redirect_field_name=None)
def mentor_form(request, slug):
    if request.method == 'POST' and request.user != ProjectDetails.objects.get(project_slug=slug).user:
        sending_user = CustomUser.objects.get(username=request.user.username)
        body = request.POST['body']
        proj = ProjectDetails.objects.get(project_slug=slug)
        mentor = Mentors()
        mentor.project_id = proj
        mentor.requesting_user = sending_user
        mentor.body = body
        mentor.save()

        mail_subject = 'New Mentorship Proposal for {}'.format(mentor.project_id.title)
        body = render_to_string('Projects/emails/mentor_mail.html', {
            'user': request.user.username,
            'Project': mentor.project_id,
            'mentor': mentor,
        })
        to_email = proj.user.email
        message = Mail(
            from_email='sameeranbandishti93@gmail.com',
            to_emails=to_email,
            subject=mail_subject,
            html_content=body
        )
        try:
            sg = SendGridAPIClient(os.environ.get('SENDGRID_API_KEY'))
            response = sg.send(message)
            print(response.status_code)
            print(response.body)
            print(response.headers)
        except Exception as e:
            print(e.message)

        return redirect('project_details', username=proj.user.username, slug=slug)

    else:
        return HttpResponse("You Can't Mentor Yourself! -_-")

@login_required(login_url='/login/', redirect_field_name=None)
def view_mentorships(request, slug):
    project = ProjectDetails.objects.get(project_slug=slug)
    mentorships = Mentors.objects.filter(project_id__project_slug=slug).filter(approved=False)

    if request.user == project.user:
        return render(request, 'Projects/mentor_view.html', {'project': project, 'mentorships': mentorships})
    else:
        return redirect('login')

@login_required(login_url='/login/', redirect_field_name=None)
def acceptmentor(request, mentor_id):
    mentor = Mentors.objects.get(mentor_id=mentor_id)
    mentor.approved = True
    mentor.save()
    hist = History()
    hist.user = request.user
    hist.project_id = mentor.project_id
    hist.type_of_action = "Mentor"
    hist.save()
    return redirect('Projects:mentorship_view', slug=mentor.project_id.project_slug)

@login_required(login_url='/login/', redirect_field_name=None)
def rejectmentor(request, mentor_id):
    mentor = Mentors.objects.get(mentor_id=mentor_id)
    slug = mentor.project_id.project_slug
    mentor.delete()
    return redirect('Projects:mentorship_view', slug=slug)

@login_required(login_url='/login/', redirect_field_name=None)
def intern_form(request, slug):
    if request.method == 'POST' and request.user != ProjectDetails.objects.get(project_slug=slug).user:
        sending_user = CustomUser.objects.get(username=request.user.username)
        body = request.POST['body']
        proj = ProjectDetails.objects.get(project_slug=slug)
        intern = Interns()
        intern.project_id = proj
        intern.requesting_user = sending_user
        intern.body = body
        intern.save()

        mail_subject = 'New Internship Proposal for {}'.format(intern.project_id.title)
        body = render_to_string('Projects/emails/intern_mail.html', {
            'user': request.user.username,
            'Project': intern.project_id,
            'intern': intern,
        })
        to_email = proj.user.email
        message = Mail(
            from_email='sameeranbandishti93@gmail.com',
            to_emails=to_email,
            subject=mail_subject,
            html_content=body
        )
        try:
            sg = SendGridAPIClient(os.environ.get('SENDGRID_API_KEY'))
            response = sg.send(message)
            print(response.status_code)
            print(response.body)
            print(response.headers)
        except Exception as e:
            print(e.message)

        return redirect('project_details', username=proj.user.username, slug=slug)

    else:
        return HttpResponse("You Can't Intern At Your Own Project! -_-")

@login_required(login_url='/login/', redirect_field_name=None)
def view_internships(request, slug):
    project = ProjectDetails.objects.get(project_slug=slug)
    internships = Interns.objects.filter(project_id__project_slug=slug).filter(approved=False)

    if request.user == project.user:
        return render(request, 'Projects/intern_view.html', {'project': project, 'internships': internships})
    else:
        return redirect('login')


@login_required(login_url='/login/', redirect_field_name=None)
def acceptintern(request, intern_id):
    intern = Interns.objects.get(intern_id=intern_id)
    intern.approved = True
    intern.save()
    hist = History()
    hist.user = request.user
    hist.project_id = intern.project_id
    hist.type_of_action = "Intern"
    hist.save()
    return redirect('Projects:internship_view', slug=intern.project_id.project_slug)

@login_required(login_url='/login/', redirect_field_name=None)
def rejectintern(request, intern_id):
    intern = Interns.objects.get(intern_id=intern_id)
    slug = intern.project_id.project_slug
    intern.delete()
    return redirect('Projects:internship_view', slug=slug)


@login_required(login_url='/login/', redirect_field_name=None)
def colab_form(request, slug):
    if request.method == 'POST' and request.user != ProjectDetails.objects.get(project_slug=slug).user:
        sending_user = CustomUser.objects.get(username=request.user.username)
        body = request.POST['body']
        proj = ProjectDetails.objects.get(project_slug=slug)
        colab = Collaborators()
        colab.project_id = proj
        colab.requesting_user = sending_user
        colab.body = body
        colab.save()

        mail_subject = 'New Collaboration Proposal for {}'.format(colab.project_id.title)
        body = render_to_string('Projects/emails/colab_mail.html', {
            'user': request.user.username,
            'Project': colab.project_id,
            'colab': colab,
        })
        to_email = proj.user.email
        message = Mail(
            from_email='sameeranbandishti93@gmail.com',
            to_emails=to_email,
            subject=mail_subject,
            html_content=body
        )
        try:
            sg = SendGridAPIClient(os.environ.get('SENDGRID_API_KEY'))
            response = sg.send(message)
            print(response.status_code)
            print(response.body)
            print(response.headers)
        except Exception as e:
            print(e.message)

        return redirect('project_details', username=proj.user.username, slug=slug)

    else:
        return HttpResponse("You Can't Collaborate With Yourself! -_-")

@login_required(login_url='/login/', redirect_field_name=None)
def view_collaborations(request, slug):
    project = ProjectDetails.objects.get(project_slug=slug)
    collaborators = Collaborators.objects.filter(project_id__project_slug=slug).filter(approved=False)

    if request.user == project.user:
        return render(request, 'Projects/colab_view.html', {'project': project, 'collaborators': collaborators})
    else:
        return redirect('login')

@login_required(login_url='/login/', redirect_field_name=None)
def acceptcolab(request, colab_id):
    colab = Collaborators.objects.get(colab_id=colab_id)
    colab.approved = True
    colab.save()
    hist = History()
    hist.user = request.user
    hist.project_id = colab.project_id
    hist.type_of_action = "Collaborator"
    hist.save()
    return redirect('Projects:internship_view', slug=colab.project_id.project_slug)

@login_required(login_url='/login/', redirect_field_name=None)
def rejectcolab(request, colab_id):
    colab = Collaborators.objects.get(collaborator_id=colab_id)
    slug = colab.project_id.project_slug
    colab.delete()
    return redirect('Projects:internship_view', slug=slug)
