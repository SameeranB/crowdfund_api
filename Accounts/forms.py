from django import forms
from django.core.exceptions import ValidationError
from Accounts.models import CustomUser


class SignupForm(forms.Form):
    username = forms.CharField(max_length=50)
    email = forms.EmailField()
    first_name = forms.CharField(max_length=50)
    last_name = forms.CharField(max_length=50)
    password = forms.CharField(max_length=20)
    confirm_password = forms.CharField(max_length=20)
    accept_terms = forms.CharField(max_length=4)


    def clean(self):
        data=super().clean()
        password = data.get("password")
        conf_password = data.get("confirm_password")
        email = data.get("email")
        if conf_password != password:
            raise ValidationError("The Passwords Do Not Match")
        username = data.get("username")
        existing_usernames = CustomUser.objects.values_list('username', flat=True)
        if username in existing_usernames:
            raise ValidationError("This Username is Already Being Used, Please Use a Different Username")

        existing_emails = CustomUser.objects.values_list('email', flat=True)
        if email in existing_emails:
            raise ValidationError("This Email Is Already In Use. Please Use A Different One")

        return data



class ProfileChangeForm(forms.Form):
    designation = forms.CharField(max_length=50, required=False)
    institute = forms.CharField(max_length=50, required=False)
    bio = forms.CharField(max_length=10000, required=False)
    website = forms.URLField(required=False)
    linkedin_handle = forms.CharField(max_length=100, required=False)
    instagram_handle = forms.CharField(max_length=100, required=False)
    twitter_handle = forms.CharField(max_length=100, required=False)
    codebase_handle = forms.CharField(max_length=100, required=False)
    user_accept_collaborations = forms.CharField(max_length=3, required=False)
    user_accept_interns = forms.CharField(max_length=3, required=False)

class AccountInfoChangeForm(forms.Form):
    username = forms.CharField(max_length=100, required=False)
    phone_no = forms.IntegerField(required=False)
    email = forms.EmailField(required=False)
    first_name = forms.CharField(max_length=100, required=False)
    last_name = forms.CharField(max_length=100, required=False)



class BankDetailsChangeForm(forms.Form):
    bank_name = forms.CharField(max_length=100)
    account_no = forms.CharField(max_length=18)
    re_account_no = forms.CharField(max_length=18)
    ifsc = forms.CharField(max_length=11)
    beneficiary = forms.CharField(max_length=100)
    pan = forms.CharField(max_length=10)

    def clean(self):
        data = super().clean()
        account_no = data.get("account_no")
        re_account_no = data.get("re_account_no")

        if re_account_no != account_no:
            raise ValidationError("The Account Numbers Don't Match")

