from django.contrib import messages
from django.contrib.auth.mixins import LoginRequiredMixin
from django.core.exceptions import ValidationError
from django.shortcuts import render, redirect
from django.views.generic import TemplateView
from django.contrib.auth.decorators import login_required
from Accounts.forms import ProfileChangeForm, AccountInfoChangeForm, BankDetailsChangeForm
from Accounts.models import CustomUser, Profile, BankDetails, Subsriptions, History
from Miscellaneous.models import FeaturedResearch, UpcomingFeatures, News
from Projects.models import ProjectDetails, Collaborators, Bookmarks, ProjectTags, Interns
from Accounts import forms as account_forms
from django.urls import reverse_lazy, reverse
from django.views import generic
from django.contrib.sites.shortcuts import get_current_site
from django.utils.encoding import force_bytes, force_text
from django.utils.http import urlsafe_base64_encode, urlsafe_base64_decode
from django.template.loader import render_to_string
from .tokens import account_activation_token
from django.contrib.auth.models import User
from django.core.mail import EmailMessage
from django.http import HttpResponse, HttpResponseRedirect
from django.contrib.auth import login, authenticate, logout
from django.contrib.auth import get_user_model
from django.conf import settings
from django.shortcuts import get_object_or_404
import os
from sendgrid import SendGridAPIClient
from sendgrid.helpers.mail import Mail
import requests

headers = {
    'developer_key': 'becbbce45f79c6f5109f848acd540567',
    'secret-key': 'MC6dKW278tBef+AuqL/5rW2K3WgOegF0ZHLW/FriZQw=',
    'secret-key-timestamp': '1516705204593',
}

User = get_user_model()

# Registration Views:

class SignupView(TemplateView):
    template_name = 'Accounts/signup.html'


def signup_form(request):
    if request.method == 'POST':
        form = account_forms.SignupForm(data=request.POST)
        if form.is_valid():
            if form.cleaned_data['accept_terms'] == 'on':
                user = CustomUser()
                user.username = form.cleaned_data['username']
                user.email = form.cleaned_data['email']
                user.set_password(form.cleaned_data['password'])
                user.first_name = form.cleaned_data['first_name']
                user.last_name = form.cleaned_data['last_name']
                user.kyc_done = False
                user.kyc_applied = False
                user.is_active = False
                user.save()
                prof = Profile()
                prof.user = CustomUser.objects.get(username__exact=form.cleaned_data['username'])
                prof.save()
                bank = BankDetails()
                bank.user = CustomUser.objects.get(username__exact=form.cleaned_data['username'])
                bank.save()
                print("user created")
                current_site = get_current_site(request)
                mail_subject = 'Activate your Oversight account'
                body = render_to_string('registration/acc_activate_email.html', {
                    'user': user,
                    'domain': current_site.domain,
                    'uid': urlsafe_base64_encode(force_bytes(user.pk)),
                    'token': account_activation_token.make_token(user),
                })
                print("body created")


                to_email = form.cleaned_data.get('email')
                message = Mail(
                    from_email="sameeranbandishti93@gmail.com",
                    to_emails=to_email,
                    subject=mail_subject,
                    html_content=body
                )
                try:
                    sg = SendGridAPIClient(os.environ.get('SENDGRID_API_KEY'))
                    print("API Key Found")
                    response = sg.send(message)
                    print(response.status_code)
                    print(response.body)
                    print(response.headers)
                except Exception as e:
                    print(e.message)

                messages.info(request, "Check inbox and confirm your email to activate your account.")
            else:
                messages.error(request, "Please accept Terms And Conditions to create account.")
            return render(request, 'Accounts/signup.html')

        else:

            temp_list = list(form.errors.values())
            print(temp_list)
            error_list = []
            for er in temp_list:
                error_list.append(er[0])
            print(error_list)
            return render(request, 'Accounts/signup.html', {'errors': error_list})




def activate(request, uidb64, token):
    try:
        uid = force_text(urlsafe_base64_decode(uidb64))
        user = User.objects.get(pk=uid)
    except(TypeError, ValueError, OverflowError):
        user = None
    if user is not None and account_activation_token.check_token(user, token):
        user.is_active = True
        user.save()
        login(request, user, backend='django.contrib.auth.backends.ModelBackend')
        # return redirect('home')
        messages.info(request, "Welcome To Oversight! We Are Glad To Have You On Board. Your Account Has Been Confirmed")
        return redirect('dashboard')
    else:
        user.delete()
        messages.error(request, "Apologies, There Was An Error With Your Confirmation. Would You Please Give It Another Shot")
        return redirect('signup')



# Login Views:

def LoginFormDef(request):
    if request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(username=username, password=password)
        if user is not None:
            if user.is_active:
                login(request, user)
                return redirect('Accounts:dashboard')
            else:
                messages.warning(request, "Your Account Seems To Have Not Been Activated. Please Check Your Mailbox and Activate Your Account")
        else:
            messages.error(request, 'Either Your Username or Password is Incorrect')

    return render(request, 'Accounts/login.html')


def LogoutView(request):
    logout(request)
    messages.info(request,"You Have Been Logged Out")
    return redirect('login')



# Dashboard Related Views:

class DashboardView( LoginRequiredMixin, TemplateView):

    login_url = '/login/'
    redirect_field_name = None

    template_name = 'Dashboard/dashboard.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        users_projects = ProjectDetails.objects.filter(user=self.request.user)
        for project in users_projects:
            # project.completion_percentage = (project.balance_amount/project.target_amount)*100.0
            project.completion_percentage = 0
            project.save()

        if users_projects is not None:
            context['Projects'] = users_projects
        user = CustomUser.objects.get(username=self.request.user.username)
        context['user'] = user
        context['FeaturedResearch'] = FeaturedResearch.objects.all()
        context['UpcomingFeatures'] = UpcomingFeatures.objects.all()
        context['News'] = News.objects.all()

        subscribed_to = Subsriptions.objects.filter(Subscriber=user).values_list('Subscribed_To__username',
                                                                                              flat=True)
        events = History.objects.filter(user__username__in=subscribed_to).order_by('time_of_action')

        context['events'] = events

        context['colabs'] = Collaborators.objects.filter(requesting_user=user)

        context['bookmarks'] = Bookmarks.objects.filter(user=user)

        context['Tags'] = ProjectTags.objects.all()

        context['Interns'] = Interns.objects.all()
        context['current_bookmarks'] = Bookmarks.objects.values_list('project_id__project_slug',flat=True).filter(user=user)
        return context


class ProfileAboutView(LoginRequiredMixin, TemplateView):

    login_url = '/login/'
    redirect_field_name = None

    template_name = 'Dashboard/profile-about.html'


class PricingView(LoginRequiredMixin, TemplateView):

    login_url = '/login/'
    redirect_field_name = None

    template_name = 'Dashboard/pricing.html'


class LandingPageView(LoginRequiredMixin, TemplateView):

    login_url = '/login/'
    redirect_field_name = None

    template_name = 'Landing/index.html'

def UserProfileView(request,username):
    try:
        viewed_user = CustomUser.objects.get(username=username)
    except Exception as e:
        return HttpResponse(e)
    try:
        operating_user = CustomUser.objects.get(username=request.user.username)
    except Exception as e:
        operating_user = None
    try:
        subslist = Subsriptions.objects.filter(Subscriber=request.user)
    except Exception as f:
        pass
    subbed = True
    try:
        subslist = subslist.get(Subscribed_To=viewed_user)
    except Exception as e:
        subbed = False
        print(e)
    # if subslist is not None:
    #     subbed = True
    # else:
    #     subbed = False

    projects = ProjectDetails.objects.filter(user=viewed_user)



    return render(request, 'Accounts/profile-new.html', {'viewed_user':viewed_user, 'user':operating_user, 'projects':projects, 'subbed': subbed})

@login_required(login_url='/login/')
def profile_change_form(request):
    viewed_username = request.user.username
    if viewed_username == viewed_username:
        if request.method == 'POST':
            form = ProfileChangeForm(request.POST)
            if form.is_valid():
                user = CustomUser.objects.get(username=viewed_username)
                prof = Profile.objects.get(user=user)
                if form.cleaned_data['designation']: prof.designation = form.cleaned_data['designation']
                if form.cleaned_data['institute']: prof.institute = form.cleaned_data['institute']
                if form.cleaned_data['bio']: prof.bio = form.cleaned_data['bio']
                if form.cleaned_data['website']:  prof.website = form.cleaned_data['website']
                if form.cleaned_data['linkedin_handle']:  prof.linkedin_handle = form.cleaned_data['linkedin_handle']
                if form.cleaned_data['instagram_handle']: prof.instagram_handle = form.cleaned_data['instagram_handle']
                if form.cleaned_data['twitter_handle']:  prof.twitter_handle = form.cleaned_data['twitter_handle']
                if form.cleaned_data['codebase_handle']:  prof.codebase_handle = form.cleaned_data['codebase_handle']
                if form.cleaned_data['user_accept_collaborations']:  user.user_accept_collaborations = form.cleaned_data['user_accept_collaborations']
                if form.cleaned_data['user_accept_interns']:  user.user_accept_interns = form.cleaned_data['user_accept_interns']
                user.save()
                prof.save()
                return redirect('Accounts:account_settings')
            else:
                return render(request, 'Accounts/profile-new.html', {'errors':form.errors})
    else:
        return HttpResponse("You are not allowed to do that")

@login_required(login_url='/login/')
def account_info_change_form(request):
    viewed_username = request.user.username
    if request.method == 'POST':
        form = AccountInfoChangeForm(request.POST)
        if form.is_valid():
            user = CustomUser.objects.get(username=viewed_username)
            if form.cleaned_data['username']:
                if form.cleaned_data['username'] in CustomUser.objects.values_list('username', flat=True):
                    user.username = form.cleaned_data['username']
                else:
                    raise ValidationError("Username Not Available")
            if form.cleaned_data['phone_no']: user.profile.phone_no = form.cleaned_data['phone_no']
            if form.cleaned_data['email']: user.email = form.cleaned_data['email']
            if form.cleaned_data['first_name']: user.first_name = form.cleaned_data['first_name']
            if form.cleaned_data['last_name']: user.last_name = form.cleaned_data['last_name']

            user.save()
            user.profile.save()
            return redirect('Accounts:account_settings')
        else:
            return render(request, 'Accounts/profile-new.html', {'errors': form.errors})
    else:
        return HttpResponse("You are not allowed to do that")

@login_required(login_url='/login/')
def bank_details_change_form(request):
    viewed_username = request.user.username
    if request.method == 'POST':
        form = BankDetailsChangeForm(request.POST)
        if form.is_valid():
            user = CustomUser.objects.get(username=viewed_username)
            if form.cleaned_data['bank_name']: user.bankdetails.bank_name = form.cleaned_data['bank_name']
            if form.cleaned_data['account_no']: user.bankdetails.account_no = form.cleaned_data['account_no']
            if form.cleaned_data['ifsc']: user.bankdetails.ifsc = form.cleaned_data['ifsc']
            if form.cleaned_data['beneficiary']: user.bankdetails.beneficiary = form.cleaned_data['beneficiary']
            if form.cleaned_data['pan']: user.bankdetails.pan = form.cleaned_data['pan']
            user.bankdetails.save()
            return redirect('Accounts:account_settings', username=viewed_username)
        else:
            return render(request, 'Accounts/profile-new.html', {'errors': form.errors})
    else:
        return HttpResponse("You are not allowed to do that")

    # data = {
    #     'pan_number': 'AMDPT3605R',
    #     'purpose': '1',
    #     'initiator_id': '9971771929',
    #     'purpose_desc': 'onboarding',
    #     'customer_mobile': '8654547658'
    # }

# response = requests.post('https://staging.eko.in:25004/ekoapi/v1/pan/verify', headers=headers, data=data)

@login_required(login_url='/login/')
def subscribe(request, viewed_user):
    subs = Subsriptions()
    subs.Subscriber = CustomUser.objects.get(username=request.user.username)
    subs.Subscribed_To = CustomUser.objects.get(username=viewed_user)
    subs.save()
    return redirect('user_profile', username=viewed_user)

@login_required(login_url='/login/')
def unsubscribe(request, viewed_user):
    subs = Subsriptions.objects.filter(Subscriber=request.user).get(Subscribed_To__username=viewed_user)
    subs.delete()
    return redirect('dashboard')
# class Feed(TemplateView):
#     template_name = 'Accounts/feed.html'
#
#     def get_context_data(self, **kwargs):
#         context = super().get_context_data(**kwargs)
#         context['user'] = self.request.user
#
#         subscribed_to = Subsriptions.objects.filter(Subscriber=self.request.user).values_list('Subscribed_To__username', flat=True)
#         events = History.objects.filter(user__username__in=subscribed_to).order_by('time_of_action')
#
#         context['events'] = events
#         return context



class AccountSettingsView(LoginRequiredMixin, TemplateView):

    login_url = '/login/'
    redirect_field_name = None

    template_name = 'Accounts/account_settings.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        user = CustomUser.objects.get(username=self.request.user.username)
        context['user'] = user
        return context


# def user_intern_form(request, username):
#     requesting_user = CustomUser.objects.get(username=request.user.username)
#     sent_to_user = CustomUser.objects.get(username=username)
#     if request.method == 'POST' and requesting_user!=sent_to_user :
#
