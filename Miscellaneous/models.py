from django.db import models
from Projects import models as project_models
# Create your models here.

class FeaturedResearch(models.Model):
    project_id = models.ForeignKey(project_models.ProjectDetails, on_delete=models.CASCADE, primary_key=True)
    date_added = models.DateTimeField(auto_now=True, auto_now_add=False)

class News(models.Model):
    news_id = models.AutoField(primary_key=True)
    hyperlink = models.URLField(max_length=100)
    body = models.CharField(max_length=100000)

class UpcomingFeatures(models.Model):
    feature_id = models.AutoField(primary_key=True)
    body = models.CharField(max_length=100000)


