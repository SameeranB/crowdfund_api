from django.contrib import admin

# Register your models here.
from Miscellaneous.models import UpcomingFeatures, News, FeaturedResearch

admin.site.register(UpcomingFeatures)
admin.site.register(News)
admin.site.register(FeaturedResearch)