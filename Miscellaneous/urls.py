from django.urls import path 
from . import views

urlpatterns = [
    # Misc URLs:
    path('searchbar/', views.searchbar, name='search_bar'),
    path('results/', views.search, name='search'),

]
