from django.db import models
from Accounts import models as account_models
from django.conf import settings
# Create your models here.

class transaction(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    amount = models.IntegerField()
    order_id = models.CharField(max_length = 90, null=True)
    payment_id = models.CharField(max_length=90, null=True)
    signature = models.CharField(max_length = 90, null=True)
    created = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.user.username