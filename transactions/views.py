from django.shortcuts import render
from django.views.generic import TemplateView
import requests
from . import models
from . import forms
import razorpay
from django.conf import settings
from django.http import HttpResponse, HttpResponseRedirect
from django.http.response import JsonResponse
from django.contrib.auth import get_user_model
from django.urls import reverse
from django.urls import reverse_lazy
from django.shortcuts import redirect
import json
from django.contrib import messages
from Accounts.models import CustomUser
from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin

User = get_user_model()
razor_key = settings.PAY_KEY
razor_secret = settings.PAY_SECRET

client = razorpay.Client(auth=(razor_key, razor_secret))


def project_split(amount):
        return round(0.91*amount)

def oversight_split(amount):
        return str(0.09*amount)


def order_specs(amount):
    client = razorpay.Client(
         auth=("rzp_test_3fKRmQXeI39w7X", "AIdqtlvLv8UCSLWH8eBFfSNd"))
    DATA = {'amount': amount,
            'currency': 'INR',
            'payment_capture': 1,
            'receipt': '1'}
    return client.order.create(data=DATA)


class PreTransactionView(TemplateView):
    template_name = 'transactions/pre_transaction.html'



def amountform(request):
    if request.method == 'POST':
        amount = request.POST['amount']
        DATA = {'amount': amount,
                'currency': 'INR',
                'payment_capture': 1,
                'receipt': '1'}
        order = client.order.create(data=DATA)
        order_id = order['id']
        print(order_id)
        return render(request, 'transactions/transactform.html',
                      {'key': razor_key, 'id': order_id, 'amount': amount})


def TransactionView(request):
        data = request.POST
        print(data) 
        transaction = models.transaction()
        transaction.user = CustomUser.objects.get(username__exact=request.user.username)
        payment_id = data['razorpay_payment_id']
        order_id = data['razorpay_order_id']
        signature = data['razorpay_signature'] 
        fetch_payment = client.payment.fetch(payment_id)
        amount = fetch_payment['amount']
        print(amount)
        project_amount = project_split(amount)
        print("project = " +str(project_amount))
        transfer_data = {
        'account' : 'acc_CUHiX4Y7zxzaHZ',
        'amount': "%s" %project_amount,
        'currency': 'INR',      
        }

        response = requests.post('https://api.razorpay.com/v1/transfers',
                                        data=transfer_data, auth=(razor_key, razor_secret))
        print(response)
        return render(request, 'transactions/transactionSuccess.html', {'data':data, 'response':response})





# def transactView(request):
#     if request.method == 'POST':
#         form = forms.TransactionForm(data=request.POST)
        
#         if form.is_valid():
#             transaction = models.transaction()
#             transaction.user = CustomUser.objects.get(
#                 username__exact=request.user.username)
#             transaction.amount = form.cleaned_data['amount']
#             transaction.order_id = order_specs(transaction.amount)
#             transaction.save()
#             return redirect(reverse('pay'), )
#         else:
#             HttpResponse('invalod')
#     else:
#         form = forms.TransactionForm()
#         return render(request, 'transactions/pre_transaction.html', {'form': form})

# def transactionView(request):
#     amount = transactView(request)
#     return HttpResponse('amount')
    # order = order_specs(amount)
    # id = order['id']
    # return HttpResponse(id)
    # return render(request, 'transactions/transactform.html', {'id':id})    







# def pre_transactionView(request):
#     if request.method == 'POST':
#         form = forms.TransactionForm(request.POST)
#         if form.is_valid():
#             amount = request.POST['amount']
#             order = order_specs(amount)
#             {'order_id': order['id']}
#             return redirect('pay')
#         else:
#             return HttpResponse('invalid form')
#     else:
#         form = forms.TransactionForm()
#         return render(request, 'transactions/transactform.html', {'form':form})




# def transactionProcess():
#     amount = forms.TransactionForm.objects.get(amount)
#     razorpay_client = razorpay.Client(auth=("<APP_ID>", "<APP_SECRET>"))



    
